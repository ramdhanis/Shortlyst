package re.rezatesting

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco

/**
 * Created by payfazz87 on 21/04/18.
 */
class App: Application() {

    override fun onCreate() {
        super.onCreate()

        // Initialize Fresco
        Fresco.initialize(this)

    }
}
