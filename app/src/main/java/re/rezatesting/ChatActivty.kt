package re.rezatesting

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomSheetDialog
import android.view.View
import kotlinx.android.synthetic.main.activity_chat.*
import android.widget.LinearLayout


class ChatActivty : AppCompatActivity(), View.OnClickListener{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        ic_setting.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        if(view === ic_setting){
            initBottomSheet()
        }
    }


    private fun initBottomSheet() {

        val mBottomSheetDialog = BottomSheetDialog(this)
        val sheetView = this.layoutInflater.inflate(R.layout.bottom_sheet, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()

         var filter = sheetView.findViewById<View>(R.id.fragment_history_bottom_sheet_filter) as LinearLayout
        filter.setOnClickListener {
            FilterActivity.launchIntent(this)
        }

    }


}
