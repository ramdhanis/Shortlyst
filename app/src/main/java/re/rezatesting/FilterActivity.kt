package re.rezatesting

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_filter.*

/**
 * Created by payfazz87 on 22/04/18.
 */
class FilterActivity : AppCompatActivity() , View.OnClickListener{


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter)
        tv_company_filter.setOnClickListener(this)
    }


    override fun onClick(view: View?) {
        if(view === tv_company_filter){
            FilterCompanyActivity.launchIntent(this)
        }
    }


    companion object {

        /**
         * Launch this activity.
         * @param context the context
         */
        fun launchIntent(ctx: Context) {
            val intent = Intent(ctx, FilterActivity::class.java)
            ctx.startActivity(intent)
        }
    }
}