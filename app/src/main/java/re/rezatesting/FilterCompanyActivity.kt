package re.rezatesting

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

/**
 * Created by payfazz87 on 23/04/18.
 */
class FilterCompanyActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter_company)
    }

    companion object {

        /**
         * Launch this activity.
         * @param context the context
         */
        fun launchIntent(ctx: Context) {
            val intent = Intent(ctx, FilterCompanyActivity::class.java)
            ctx.startActivity(intent)
        }
    }
}