package re.rezatesting

import android.app.Activity
import android.view.View
import android.view.ViewConfiguration
import android.view.WindowManager

/**
 * Created by payfazz87 on 21/04/18.
 */
class TransculentUtils {
    private var activity: Activity? = null

    fun attachActivity(activity: Activity) {
        this.activity = activity
        val window = activity.window
        window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
        // setViewAboveNavigationBar(window.getDecorView());
    }

    fun setContainerView(view: View) {
        setViewBellowStatusBar(view)
        setViewAboveNavigationBar(view)
    }

    fun setViewBellowStatusBar(view: View) {
        var paddingLeft = view.paddingLeft
        var paddingTop = view.paddingTop + getStatusBarHeight(activity)
        var paddingRight = view.paddingRight
        var paddingBottom = view.paddingBottom
        view.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
//        view.setPadding(0, getStatusBarHeight(activity), 0, 0)
    }

    fun setViewAboveNavigationBar(view: View) {
        var paddingLeft = view.paddingLeft
        var paddingTop = view.paddingTop
        var paddingRight = view.paddingRight
        var paddingBottom = view.paddingBottom + getNavigationBarHeight(activity)
        view.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
//        view.setPadding(0, 0, 0, getNavigationBarHeight(activity))
    }

    fun getStatusBarHeight(activity: Activity?): Int {
        var result = 0
        val resourceId = activity!!.resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = activity.resources.getDimensionPixelSize(resourceId)
        }
        return result
    }

    fun getNavigationBarHeight(activity: Activity?): Int {
        var result = 0
        val hasMenuKey = ViewConfiguration.get(activity).hasPermanentMenuKey()
        val resourceId = activity!!.resources.getIdentifier("navigation_bar_height", "dimen", "android")
        if (resourceId > 0 && !hasMenuKey) {
            result = activity.resources.getDimensionPixelSize(resourceId)
        }
        return result
    }
}